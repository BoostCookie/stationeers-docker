#!/bin/sh

docker run curlimages/curl -L "https://gitlab.com/BoostCookie/stationeers-docker/-/jobs/artifacts/main/raw/changenumber.txt?job=rebuild_without_cache" > changenumber.txt
PREVIOUS_CHANGENUMBER=$(cat changenumber.txt)

echo "PREVIOUS CHANGENUMBER:"
echo $PREVIOUS_CHANGENUMBER

docker run cm2network/steamcmd ./steamcmd.sh +login anonymous +app_info_print 600760 +exit > query.txt
CURRENT_CHANGENUMBER=$(sed -n -e 's/AppID : 600760, change number : \(.*\)\/.*/\1/p' query.txt)

echo "CURRENT CHANGENUMBER:"
echo $CURRENT_CHANGENUMBER

[ "$PREVIOUS_CHANGENUMBER" = "$CURRENT_CHANGENUMBER" ] && echo "CHANGENUMBERS ARE EQUAL" && exit

echo "CHANGENUMBERS DIFFER"

echo $CURRENT_CHANGENUMBER > changenumber.txt

docker build --tag tempimage:temp .
docker tag tempimage:temp $CI_GL_REGISTRY_IMAGE:latest
docker tag tempimage:temp $CI_DH_REGISTRY_IMAGE:latest
echo -n $CI_GL_REGISTRY_TOKEN | docker login -u "$CI_GL_REGISTRY_USER" --password-stdin $CI_GL_REGISTRY
docker push $CI_GL_REGISTRY_IMAGE:latest
echo -n $CI_DH_REGISTRY_TOKEN | docker login -u "$CI_DH_REGISTRY_USER" --password-stdin $CI_DH_REGISTRY
docker push $CI_DH_REGISTRY_IMAGE:latest
