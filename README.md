# stationeers-docker

Docker setup for a dedicated server of the game [Stationeers](https://store.steampowered.com/app/544550/Stationeers/).

## Setup
```sh
git clone "https://gitlab.com/BoostCookie/stationeers-docker.git"
cd stationeers-docker
```
Edit the `settings/setting.xml` to your liking and set the worldtype in `compose.yml`.
The default is `moon`. The possible values are
- moon
- mars
- europa
- europa2
- mimas
- vulcan
- vulcan2
- space
- loulan
- venus

Next make sure the mounted directories can be accessed by the `steam` user in the image (uid 1000, gid 1000)
```sh
chown -R 1000:1000 settings saves
```

Now we can build the image and start it up with
```sh
docker-compose up
```

## Detach
Detach from `docker-compose up` via Ctrl+z and then type in `bg` to let it continue in the background.
Alternative already start in detached mode via `docker-compose up -d`.

## Stop
Stop the container with `docker stop stationeers`

## Updates
Update the container via
```sh
docker exec stationeers /home/steam/steamcmd/steamcmd.sh +force_install_dir /home/steam/stationeers/ +login anonymous +app_update 600760 validate +quit
docker restart stationeers
```

Or alternative update the whole image
```sh
docker-compose down
docker rmi stationeers-docker_stationeers
docker-compose build --no-cache
docker-compose up --build --force-recreate --no-deps
```
though this will redownload the entire game.

## Is My Server Online?
Usually when the server is working it prints on the screen something like this:
```json
>                                                                                                                                                                                                                                                                              s
Log: connection {1} has been disconnected by timeout; address {<redacted>} time {<redacted>}, last rec time {<redacted>} rtt {0} timeout {2000}
  "mapName": "Moon",
  "port": 27016,
  "password": false,
  "maxPlayers": 10,
  "ipAddress": "<redacted>"
}
```
Note that the `ipAddress` is only correct if your router uses UPnP, I think.
If it is incorrect then you cannot connect to the server by simply clicking on your server name.
Instead you need to use your actual `<ipv4address>` and connect via manually entering `<ipv4address>:27016`.
If you've changed the GamePort in `compose.yml` and/or `settings/setting.xml` you need to switch out 27016 appropriately.

You can also test the connection with
```sh
xxd -r -p <<< "0000013e3b14e70001000001000300bf2bcbc0" | nc -u <host> <gameport(27016 by default)> | xxd
```
The packet is captured from a real connection attempt, I have no idea what's in it.
If some random garbage appears on your screen, the server should be working.

## I fear docker-compose and just want an image
You should mount a directory `saves` and a directory `settings`.
The latter one should contain the `setting.xml` with important settings like the server name and password.
The following commands should work if you use `curl` for downloading and `nano` for editing text:
```sh
mkdir -p saves settings
curl -O --output-dir ./settings/ "https://gitlab.com/BoostCookie/stationeers-docker/-/raw/main/settings/setting.xml"
nano ./settings/setting.xml
chown -R 1000:1000 ./saves ./settings
docker run --detach --interactive --tty --name stationeers --publish 27015:27015/udp --publish 27016:27016/udp --volume "$PWD/saves/":/home/steam/stationeers/saves/ --volume "$PWD/settings/":/home/steam/stationeers/settings/ registry.gitlab.com/boostcookie/stationeers-docker -loadlatest -load default moon
```
